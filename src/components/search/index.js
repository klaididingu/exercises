import React, { useEffect, useState } from "react";
import ListItemsJson from "../list-items-json";

const initialList = [
	{ name: "zxzcxxc", surname: "qweqwe", age: "45" },
	{ name: "asdasd", surname: "asda", age: "14" },
];

const Search = () => {
	const [list] = useState(initialList);
	const [filteredList, setFilteredList] = useState([]);
	const [search, setSearch] = useState("");

	// useEffect -> on change te "search" filtro listen sipas name
	useEffect(() => {
		if (search) {
			const filteredList = list.filter((item) => item.name.indexOf(search) !== -1);
			setFilteredList(filteredList);
		} else {
			setFilteredList(list);
		}
	}, [search]);

	const handleChangeSearch = (e) => {
		setSearch(e.target.value);
	};

	return (
		<div>
			{/* transform to controlled input by "search" state */}
			<input type="text" placeholder="Search by name" value={search} onChange={handleChangeSearch} />

			<ListItemsJson items={filteredList} />
		</div>
	);
};

export default Search;
