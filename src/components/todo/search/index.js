import React, { useState } from "react";

const Search = ({ handleSearch = () => {} }) => {
	const [searchString, setSearchString] = useState("");

	const doSearch = () => {
		handleSearch(searchString);
	};

	const handleChangeSearch = (e) => {
		setSearchString(e.target.value);
	};

	return (
		<div>
			<div>Search</div>
			<input type="text" value={searchString} onChange={handleChangeSearch} />
			<button onClick={doSearch}>Search</button>
		</div>
	);
};

export default Search;
