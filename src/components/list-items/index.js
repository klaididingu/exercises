import React from "react";
import ListItem from "./listItem";

import styles from "./style.module.css";

const ListItems = (props) => {
	return (
		<ul className={styles.ul}>
			{props.items.map((item, index) => (
				<ListItem className={index % 2 === 0 ? styles.red : styles.black} key={index}>
					{item}
				</ListItem>
			))}
		</ul>
	);
};

export default ListItems;
