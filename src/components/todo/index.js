import React, { useEffect, useState } from "react";
import Search from "./search";
import TodoList from "./list";
import axios from "axios";

import styles from "./style.module.css";
import CreateTodo from "./create-todo";

const ENDPOINT = "https://jsonplaceholder.typicode.com/todos";

const Todo = () => {
	const [todos, setTodos] = useState([]);
	const [filteredTodos, setFilteredTodos] = useState([]);
	const [searchStr, setSearchStr] = useState("");

	const fetchTodos = async () => {
		const { data, status } = await axios.get(ENDPOINT);
		if (status === 200) {
			setTodos(data);
			setFilteredTodos(data);
		}
	};

	const handleSearch = (searchString) => {
		setSearchStr(searchString);
	};

	const clearSearch = () => {
		setFilteredTodos(todos);
	};

	const handleUpdate = async (id, data) => {
		const { status } = await axios.put(`${ENDPOINT}/${id}`, data);
		if (status === 200) {
			alert("Updated!");
		} else {
			alert("Update error!");
		}
	};

	const handleDelete = async (id) => {
		const { status } = await axios.delete(`${ENDPOINT}/${id}`);
		if (status === 200) {
			removeFromState(id);
		}
	};

	const handleCreate = async (title) => {
		const { status, data } = await axios.post(`${ENDPOINT}`, { title });
		if (status === 201) {
			insertToState(data);
		}
	};

	const insertToState = (todo) => {
		const newState = [todo, ...todos];
		setTodos(newState);
	};

	const removeFromState = (id) => {
		let newState = [...todos];
		newState = newState.filter((todo) => todo.id != id);
		setTodos(newState);
	};

	const updateFilteredTodos = () => {
		if (searchStr) {
			setFilteredTodos(todos.filter((todo) => todo.title.indexOf(searchStr) !== -1));
		} else {
			clearSearch();
		}
	};

	useEffect(() => {
		fetchTodos();
	}, []);

	useEffect(() => {
		updateFilteredTodos();
	}, [searchStr]);

	return (
		<div className={styles.todoWrapper}>
			<CreateTodo handleCreate={handleCreate} />
			<Search handleSearch={handleSearch} />
			<TodoList todos={filteredTodos} handleUpdate={handleUpdate} handleDelete={handleDelete} />
		</div>
	);
};

export default Todo;
