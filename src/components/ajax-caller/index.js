import React, { useEffect, useState } from "react";
import axios from "axios";
import UsersData from "../users-data";
import CreateUserForm from "../create-user-form";

const ENDPOINT = "https://jsonplaceholder.typicode.com/users";

const AjaxCaller = () => {
	const [data, setData] = useState([]);

	const handleInsertUser = (user) => {
		console.log("handleInsertUser", user);

		const newState = [...data];
		newState.push(user);
		setData(newState);
	};

	const removeUser = (id) => {
		const newState = [...data];

		setData(newState.filter((user) => user.id != id));
	};

	useEffect(() => {
		// fetch te dhenat
		const getData = async () => {
			const { data, status } = await axios.get(ENDPOINT);
			console.log("response", data, status);

			if (status === 200) {
				// save data to component state
				setData(data);
			}
		};

		getData();
	}, []);

	return (
		<div>
			response data:
			<UsersData users={data} removeUser={removeUser} />
			<CreateUserForm insertUser={handleInsertUser} />
		</div>
	);
};

export default AjaxCaller;
