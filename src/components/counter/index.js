import React, { useState } from "react";

const Counter = () => {
	const [counter, setCounter] = useState(0);

	const handleIncrement = () => {
		// callback mer si prop vleren aktuale te state
		// si return duhet te ktheje vleren e re te state
		setCounter((counter) => counter + 1);
	};

	const handleDecrement = () => {
		setCounter(counter - 1);
	};

	// setInterval cdo 1 sec i cili inkrementon counter me 1
	const activateIncrement = () => {
		setInterval(() => {
			handleIncrement();
		}, 1000);
	};

	return (
		<div>
			<div>Counter: {counter}</div>
			<div>
				<button onClick={handleIncrement}>Increment</button>
				<button onClick={handleDecrement}>Decrement</button>
			</div>
			<div>
				<button onClick={activateIncrement}>Start timer</button>
			</div>
		</div>
	);
};

export default Counter;
