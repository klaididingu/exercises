import React from "react";
import axios from "axios";
import styles from "./style.module.css";

const ENDPOINT = "https://jsonplaceholder.typicode.com/users";

const UsersData = ({ users = [], removeUser = () => {} }) => {
	const handleSubmit = (e) => {
		e.preventDefault();
		const { userID, name, email, website } = e.target;

		console.log("name", name);

		const id = userID.value;
		const newData = {
			name: name.value,
			email: email.value,
			website: website.value,
		};

		doUpdate(id, newData);
	};

	const doUpdate = async (id, userData) => {
		const { status, data } = await axios.put(`${ENDPOINT}/${id}`, userData);
		if (status === 200) {
			console.log("data", data);
			alert("Update success");
		} else {
			alert("Update error...");
		}
	};

	const handleDelete = (e) => {
		doDelete(e.target.dataset.userid);
	};

	const doDelete = async (id) => {
		const { status } = await axios.delete(`${ENDPOINT}/${id}`);
		if (status === 200) {
			removeUser(id);
		}
	};

	console.log("INIT USER DATA", users);

	return (
		<div className={styles.wrapper}>
			{users.map((user) => (
				<div className={styles.card} key={user.id}>
					<form onSubmit={handleSubmit}>
						<input type="hidden" name="userID" defaultValue={user.id} />
						<input type="text" required name="name" defaultValue={user.name} />
						<input type="text" required name="email" defaultValue={user.email} />
						<input type="text" required name="website" defaultValue={user.website} />
						<input type="submit" />
					</form>
					<button data-userid={user.id} onClick={handleDelete}>
						Delete
					</button>
				</div>
			))}
		</div>
	);
};

export default UsersData;
