import React from "react";
import ListItems from "../list-items";

import styles from "./style.module.css";

const list = ["dog", "cat", "chicken", "cow", "sheep", "horse"];

const List = () => {
	return (
		<div className={styles.mainDiv}>
			<ListItems items={list} />
		</div>
	);
};

export default List;
