import React from "react";
import { Link } from "react-router-dom";

const Navigation = () => {
	return (
		<div style={{ display: "flex", gap: "5px" }}>
			<Link to="/counter">Counter</Link>
			<Link to="/list">List</Link>
			<Link to="/search">Search</Link>
			<Link to="/ajax">AjaxCaller</Link>
			<Link to="/todo">Todo</Link>
		</div>
	);
};

export default Navigation;
