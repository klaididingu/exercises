import React from "react";

import styles from "./style.module.css";

const ListItemJson = (props) => {
	return <li className={`${props.className} ${styles.li}`.trim()}>{props.children}</li>;
};

export default ListItemJson;
