import axios from "axios";
import React, { useState } from "react";

const ENDPOINT = "https://jsonplaceholder.typicode.com/users";

const CreateUserForm = ({ insertUser = () => {} }) => {
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [website, setWebsite] = useState("");

	const handleChangeName = (e) => {
		setName(e.target.value);
	};
	const handleChangeEmail = (e) => {
		setEmail(e.target.value);
	};
	const handleChangeWebsite = (e) => {
		setWebsite(e.target.value);
	};

	const handleCreateUser = (e) => {
		e.preventDefault();
		const payload = {
			name,
			email,
			website,
		};
		doCreate(payload);
	};
	const doCreate = async (user) => {
		const response = await axios.post(ENDPOINT, user);

		if (response.status === 201) {
			console.log("new user", response.data);
			insertUser(response.data);
		} else {
			alert("Could not create.");
		}
	};

	return (
		<div>
			<form onSubmit={handleCreateUser}>
				<div>
					<label htmlFor="name">Name</label>
					<input id="name" type="text" value={name} onChange={handleChangeName} />
				</div>

				<div>
					<label htmlFor="email">Email</label>
					<input id="email" type="text" value={email} onChange={handleChangeEmail} />
				</div>

				<div>
					<label htmlFor="website">Website</label>
					<input id="website" type="text" value={website} onChange={handleChangeWebsite} />
				</div>

				<input type="submit" />
			</form>
		</div>
	);
};

export default CreateUserForm;
