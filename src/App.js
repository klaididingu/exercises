import { Routes, Route } from "react-router-dom";

import "./App.css";
import Counter from "./components/counter";
import List from "./components/list";

import "./App.css";
import Form from "./components/form";
import Search from "./components/search";
import AjaxCaller from "./components/ajax-caller";
import Todo from "./components/todo";
import NotFound from "./components/404";
import Navigation from "./components/navigation";

function App() {
	return (
		<>
			<Navigation />

			<div className="App">
				<Routes>
					<Route path="/counter" element={<Counter />} />
					<Route path="/list" element={<List />} />
					<Route path="/form" element={<Form />} />
					<Route path="/search" element={<Search />} />
					<Route path="/ajax" element={<AjaxCaller />} />
					<Route path="*" element={<NotFound />} />
					<Route path="/todo" element={<Todo />} />
				</Routes>
			</div>
		</>
	);
}

export default App;
