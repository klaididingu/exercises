import React from "react";
import ListItemJson from "./listItemsJson";

import styles from "./style.module.css";

const ListItemsJson = (props) => {
	return (
		<ul className={styles.ul}>
			{props.items.map((item, index) => (
				<ListItemJson className={index % 2 === 0 ? styles.red : styles.black} key={index}>
					<div>Name: {item.name}</div>
					<div>Surname: {item.surname}</div>
					<div>Age: {item.age}</div>
				</ListItemJson>
			))}
		</ul>
	);
};

export default ListItemsJson;
