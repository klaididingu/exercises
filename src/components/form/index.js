import React, { useEffect, useState } from "react";
import ListItems from "../list-items";
import ListItemsJson from "../list-items-json";

const Form = () => {
	// implement state [name, surname]
	const [name, setName] = useState("");
	const [surname, setSurname] = useState("");
	const [age, setAge] = useState("");
	const [saves, setSaves] = useState([]);
	const [savesJSON, setSavesJSON] = useState([]);

	const [search, setSearch] = useState("");
	const [filteredSaves, setFilteredSaves] = useState(saves);
	const [filteredSavesJSON, setFilteredSavesJSON] = useState(savesJSON);

	// effects
	useEffect(() => {
		console.log("Form search updated", search);
		setFilteredData();
	}, [search, saves, savesJSON]);

	const setFilteredData = () => {
		if (search) {
			setFilteredSaves(saves.filter((item) => item.indexOf(search) !== -1));
			setFilteredSavesJSON(savesJSON.filter((item) => item.name.indexOf(search) !== -1));
		} else {
			setFilteredSaves(saves);
			setFilteredSavesJSON(savesJSON);
		}
	};

	// implement setters for state
	const handleSubmit = (e) => {
		e.preventDefault();
		saveData({ name, surname });
		saveDataJSON({ name, surname, age });
	};

	const handleChangeName = (e) => {
		setName(e.target.value);
	};
	const handleChangeSurname = (e) => {
		setSurname(e.target.value);
	};
	const handleChangeAge = (e) => {
		setAge(e.target.value);
	};
	const handleSearchChange = (e) => {
		setSearch(e.target.value);
	};

	const saveData = (data) => {
		// save submitted data in a state "saves"
		// "saves" esht nje array me strings
		const newState = [...saves];
		newState.push(`${data.name} ${data.surname}`);
		setSaves(newState);
	};

	const saveDataJSON = ({ name, surname, age }) => {
		const newState = [...savesJSON];
		newState.push({ name, surname, age });
		setSavesJSON(newState);
	};

	return (
		<div>
			<form onSubmit={handleSubmit}>
				<input type="text" placeholder="Name" value={name} onChange={handleChangeName} />
				<input type="text" placeholder="Surname" value={surname} onChange={handleChangeSurname} />
				<input type="text" placeholder="Age" value={age} onChange={handleChangeAge} />
				<input type="submit" value="Save" />
			</form>

			<div>
				<input type="text" placeholder="Search saves" value={search} onChange={handleSearchChange} />

				<hr />

				<h6>saved submits</h6>
				{/* "saves" perdoret nga <ListItems /> qe ndertuam me para */}
				<ListItems items={filteredSaves} />

				<hr />

				<h6>saved submits json</h6>
				{/* "savesJSON" perdoret nga <ListItemsJson /> si prop. 
                    <ListItemsJson /> printon nje <ListItemJson /> per cdo object ne array "saves" */}
				<ListItemsJson items={filteredSavesJSON} />
			</div>
		</div>
	);
};

export default Form;
