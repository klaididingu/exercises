import React, { useState } from "react";

import styles from "../style.module.css";

const CreateTodo = ({ handleCreate = () => {} }) => {
	const [title, setTitle] = useState("");

	const handleChange = (e) => {
		setTitle(e.target.value);
	};

	const doCreate = () => {
		handleCreate(title);
	};

	return (
		<div className={styles.createWrapper}>
			<div>Create</div>
			<input type="text" value={title} onChange={handleChange} />
			<button onClick={doCreate}>Insert</button>
		</div>
	);
};

export default CreateTodo;
