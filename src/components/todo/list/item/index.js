import React, { useRef, useState } from "react";

import styles from "../../style.module.css";

const TodoItem = ({ todo, handleDelete, handleUpdate }) => {
	const [done, setDone] = useState(todo.completed);

	const titleEl = useRef();

	const doDelete = (e) => {
		handleDelete(todo.id);
	};

	const doUpdate = (e) => {
		const data = {
			completed: done,
			title: titleEl.current.value,
		};

		handleUpdate(todo.id, data);
	};

	const toggleDone = () => {
		setDone(!done);
	};

	return (
		<div className={styles.listItem}>
			<div>
				<div onClick={toggleDone} className={`${styles.completed} ${done ? styles.done : styles.doing}`.trim()}></div>
			</div>
			<div>
				<input ref={titleEl} className={styles.title} type="text" defaultValue={todo.title} />
			</div>
			<div>
				<button onClick={doDelete}>Delete</button>
				<button onClick={doUpdate}>Update</button>
			</div>
		</div>
	);
};

export default TodoItem;
