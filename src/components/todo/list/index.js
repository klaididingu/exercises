import React from "react";

import styles from "../style.module.css";
import TodoItem from "./item";

const List = ({ todos = [], handleUpdate = () => {}, handleDelete = () => {} }) => {
	return <div className={styles.listWrapper}>{todos.length ? todos.map((todo) => <TodoItem key={todo.id} handleUpdate={handleUpdate} handleDelete={handleDelete} todo={todo} />) : ""}</div>;
};

export default List;
